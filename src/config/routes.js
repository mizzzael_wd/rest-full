const express = require("express");

module.exports = server => {
  //API Routes

  const router = express.Router();
  server.use("/api", router);

  //Albatroz Routes
  const albatrozService = require("../../api/albatroz/albatrozService.js");
  albatrozService.register(router, "/albatroz");

  //Users Routes
  // {
  //   name: String,
  //   level: Number,
  //   email: String,
  //   phone: String,
  //   common: String,
  //   password: String
  // }
  const usersService = require("../../api/users/usersService.js");
  usersService.register(router, "/users");
};
