const restfull = require("node-restful");
const mongoose = restfull.mongoose;

const usersSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  level: {
    type: Number,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: false
  },
  common: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  done: {
    type: Boolean,
    required: true,
    default: false
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

module.exports = restfull.model("Users", usersSchema);
