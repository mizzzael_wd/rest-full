const Albatroz = require("./Albatroz");

Albatroz.methods(["get", "post", "put", "delete"]);
Albatroz.updateOptions({ new: true, runValidators: true });

module.exports = Albatroz;
